package com.imanage.hardware;

import jnt.scimark2.Constants;
import jnt.scimark2.Random;
import jnt.scimark2.kernel;
import org.apache.commons.cli.*;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PerformanceTest implements Runnable {

    private static int  NUM_TESTS = 1000;
    private static boolean verbose = false;

    public void run(){

        for (int i = 0; i < NUM_TESTS; i++) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date();
            double[] res = skiTest();
            res[0] = (res[1] + res[2] + res[3] + res[4] + res[5]) / 5;
            System.out.println(formatter.format(date) + " - " + Thread.currentThread().getName() + " - Score: " + Math.round(res[0]) + " Mflops (Millions of floating point operations per second.)");
            if(verbose) {
                System.out.print(formatter.format(date) + " - " + Thread.currentThread().getName() + " - FFT ");
                if (res[1]==0.0)
                    System.out.println(" ERROR, INVALID NUMERICAL RESULT!");
                else
                    System.out.println(res[1]);

                System.out.println(formatter.format(date) + " - " + Thread.currentThread().getName() + " - SOR " + res[2]);
                System.out.println(formatter.format(date) + " - " + Thread.currentThread().getName() + " - Monte Carlo : " + res[3]);
                System.out.println(formatter.format(date) + " - " + Thread.currentThread().getName() + " - Sparse matmult " + res[4]);
                System.out.print(formatter.format(date) + " - " + Thread.currentThread().getName() + " - LU : ");
                if (res[5]==0.0)
                    System.out.println(" ERROR, INVALID NUMERICAL RESULT!");
                else
                    System.out.println(res[5]);
            }
        }
    }

    private static void setNumTest(int numTest){
        NUM_TESTS = numTest;
    }

    private static void setVerbose(boolean verb){
        verbose = verb;
    }

    public static void main(String[] args){
        CommandLineParser parser = new DefaultParser();
        Option numberOfThreads = OptionBuilder.withArgName("threads")
                .hasArg()
                .withDescription("Number of Threads to run the test with")
                .create("numberOfThreads");
        Option cycles = OptionBuilder.withArgName("cycles")
                .hasArg()
                .withDescription("Number of times the test should run per thread. Default 1000.")
                .create("cycles");
        Option verbose = OptionBuilder.withArgName("verbose")
                .hasArg()
                .withDescription("Show more verbose information")
                .create("verbose");
        Options options = new Options();
        options.addOption(numberOfThreads);
        options.addOption(cycles);
        options.addOption(verbose);

        int numThreads = 1;
        int numTests = 1000;
        boolean verboseB = false;
        try {
            CommandLine line = parser.parse(options, args);
            if(line.hasOption("numberOfThreads")){
                numThreads = Integer.parseInt(line.getOptionValue("numberOfThreads"));
            }
            if(line.hasOption("cycles")){
                numTests = Integer.parseInt(line.getOptionValue("cycles"));
                setNumTest(numTests);
            }
            if(line.hasOption("verbose")){
                verboseB = Boolean.parseBoolean(line.getOptionValue("verbose"));
                setVerbose(verboseB);
            }

        } catch (ParseException exp) {

        }
        System.out.println("Running with "+numThreads+" Threads.");
        for (int j =0; j < numThreads; j++) {
            PerformanceTest pfTest = new PerformanceTest();
            Thread one = new Thread(pfTest);
            one.start();
        }
        //System.out.println("Took " + (System.nanoTime()-start)/1000000 + "ms (expected " + (NUM_TESTS*milliseconds) + ")");*/


    }

    public static double[] skiTest(){
        double min_time = Constants.RESOLUTION_DEFAULT;

        int FFT_size = Constants.FFT_SIZE;
        int SOR_size =  Constants.SOR_SIZE;
        int Sparse_size_M = Constants.SPARSE_SIZE_M;
        int Sparse_size_nz = Constants.SPARSE_SIZE_nz;
        int LU_size = Constants.LU_SIZE;

         // run the benchmark
        FFT_size = Constants.LG_FFT_SIZE;
        SOR_size =  Constants.LG_SOR_SIZE;
        Sparse_size_M = Constants.LG_SPARSE_SIZE_M;
        Sparse_size_nz = Constants.LG_SPARSE_SIZE_nz;
        LU_size = Constants.LG_LU_SIZE;


        double res[] = new double[6];
        Random R = new Random(Constants.RANDOM_SEED);

        res[1] = kernel.measureFFT( FFT_size, min_time, R);
        res[2] = kernel.measureSOR( SOR_size, min_time, R);
        res[3] = kernel.measureMonteCarlo(min_time, R);
        res[4] = kernel.measureSparseMatmult( Sparse_size_M,
                Sparse_size_nz, min_time, R);
        res[5] = kernel.measureLU( LU_size, min_time, R);


        res[0] = (res[1] + res[2] + res[3] + res[4] + res[5]) / 5;
        return res;

        // print out results

       /* System.out.println();
        System.out.println("SciMark 2.0a");
        System.out.println();
        System.out.println("Composite Score: " + res[0]);
        System.out.print("FFT ("+FFT_size+"): ");
        if (res[1]==0.0)
            System.out.println(" ERROR, INVALID NUMERICAL RESULT!");
        else
            System.out.println(res[1]);

        System.out.println("SOR ("+SOR_size+"x"+ SOR_size+"): "
                + "  " + res[2]);
        System.out.println("Monte Carlo : " + res[3]);
        System.out.println("Sparse matmult (N="+ Sparse_size_M+
                ", nz=" + Sparse_size_nz + "): " + res[4]);
        System.out.print("LU (" + LU_size + "x" + LU_size + "): ");
        if (res[5]==0.0)
            System.out.println(" ERROR, INVALID NUMERICAL RESULT!");
        else
            System.out.println(res[5]);

        // print out System info
        /*System.out.println();
        System.out.println("java.vendor: " +
                System.getProperty("java.vendor"));
        System.out.println("java.version: " +
                System.getProperty("java.version"));
        System.out.println("os.arch: " +
                System.getProperty("os.arch"));
        System.out.println("os.name: " +
                System.getProperty("os.name"));
        System.out.println("os.version: " +
                System.getProperty("os.version"));*/

    }

}
