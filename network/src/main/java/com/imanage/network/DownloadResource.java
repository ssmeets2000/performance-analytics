package com.imanage.network;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.UUID;

@Path("download/{size}")
public class DownloadResource {

    @GET
    @Produces(MediaType.WILDCARD)
    public Response helloWorld(@PathParam("size") String size) {
        //String json = //convert entity to json
        String json = "";
        UUID uuid = UUID.randomUUID();

        if(size == null || size.isEmpty()){
            json = "{ \"error\" : \" Specify the size in kilobytes \" }";
        } else {
            try {
                File file = createFile(uuid.toString(),Integer.parseInt(size)*1024);
                return Response.ok(new DeleteOnCloseFileInputStream(file), MediaType.WILDCARD).build();
            } catch (IOException e) {
                json = "{ \"error\" : \" " + e.getLocalizedMessage() + " \" }";
                e.printStackTrace();
            }
        }

        return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private File createFile(final String filename, final long sizeInBytes) throws IOException {
        File file = new File(filename);
        file.createNewFile();

        RandomAccessFile raf = new RandomAccessFile(file, "rw");
        raf.setLength(sizeInBytes);
        raf.close();

        return file;
    }
}
