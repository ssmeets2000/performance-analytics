package com.imanage.network;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.cli.*;
import org.glassfish.jersey.servlet.ServletContainer;

public class PerformanceTest {

    private static final String JERSEY_SERVLET_NAME = "jersey-container-servlet";

    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        Option portArg = OptionBuilder.withArgName("port")
                .hasArg()
                .withDescription("Webservice port")
                .create("port");
        Options options = new Options();
        options.addOption(portArg);

        String port = "8080";

        try {
            CommandLine line = parser.parse(options, args);
            if(line.hasOption("port")){
                port = line.getOptionValue("port");
            }

        } catch (ParseException exp) {

        }



        new PerformanceTest().start(port);
    }

    void start(String port) throws Exception {

        if (port == null || port.isEmpty()) {
            port = "8080";
        }

        String contextPath = "";
        String appBase = ".";

        Tomcat tomcat = new Tomcat();
        tomcat.setPort(Integer.valueOf(port));
        tomcat.getHost().setAppBase(appBase);

        Context context = tomcat.addContext(contextPath, appBase);
        Tomcat.addServlet(context, JERSEY_SERVLET_NAME,
                new ServletContainer(new JerseyConfiguration()));
        context.addServletMappingDecoded("/api/*", JERSEY_SERVLET_NAME);

        tomcat.start();
        tomcat.getServer().await();
    }

}
